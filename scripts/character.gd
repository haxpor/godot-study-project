extends RigidBody2D

var is_applied_force = false
const FORCE = 100
onready var constants = get_node("/root/autoload_constants")
onready var joypad = get_node("/root/autoload_joypad")
onready var ground_ray = get_node("ground_ray")

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_fixed_process(true)
	
	# ignore raycast to itself
	ground_ray.add_exception(self)

func _fixed_process(delta):
	update_control(delta)

func update_control(delta):
	# reset flag
	if ground_ray.is_colliding():
		is_applied_force = false
	
	# only when character is on the ground
	# so provide a chance for players to control character
	if ground_ray.is_colliding() && !is_applied_force:
		# calculate the result movement vector
		var movement_vector = Vector2(0,0)
		
		if Input.is_action_pressed("ui_left"):
			movement_vector += Vector2(-1,0)
		if Input.is_action_pressed("ui_right"):
			movement_vector += Vector2(1,0)
		if Input.is_action_pressed("ui_up"):
			movement_vector += Vector2(0,-1)
		
		# apply impulse force
		# we could directly check float value agains 0.0 here
		if movement_vector.x != 0.0 || movement_vector.y != 0.0:
			print("Apply force")
			apply_impulse(Vector2(0,0), movement_vector * FORCE)
			is_applied_force = true

# get persisting data from this node
func get_persist_data():
	return {
		type=get_type(),
		filename=get_filename(),
		parent=get_parent().get_path(),
		position_x=get_pos().x,
		position_y=get_pos().y,
		rotation=get_rot(),
		scale_x=get_scale().x,
		scale_y=get_scale().y
	}