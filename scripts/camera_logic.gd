extends Node

var old_cam_pos

onready var screen_size = get_viewport().get_rect().size

# get following node 
export(NodePath) var following_node_nodepath
onready var following_node = get_node(following_object_nodepath)

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	#print(screen_size)
	set_fixed_process(true)
	
func _fixed_process(delta):
	update_camera(delta)

func update_camera(delta):
	