extends Node

var is_joy_connected = false

func _ready():
	# initial detect any connected joys, and set flag as such
	_detect_first_joy_connected()
	
	# register joy changed event
	Input.connect("joy_connection_changed", self, "_on_joy_connection_changed")

func _detect_first_joy_connected():
	# detect any connected joy
	var connected_joy_ids = Input.get_connected_joysticks()
	if connected_joy_ids.size() > 0:
		print("Detected " + Input.get_joy_name(connected_joy_ids[0]) + " connected")
		is_joy_connected = true

# detect changed in joy connection
func _on_joy_connection_changed(device_id, connected):
	if connected && !is_joy_connected:
		_detect_first_joy_connected()
	elif !connected:
		is_joy_connected = false
		